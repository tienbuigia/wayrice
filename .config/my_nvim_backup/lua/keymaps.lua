-- shorten
local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set

-- basic
keymap({ "n", "v" }, "<Space>", "<Nop>", { silent = true })
keymap("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
keymap("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
keymap({ "n", "v" }, "<Esc>", "<Esc>:nohl<CR>", { silent = true })

-- edit dupplicate word, sentence
keymap("n", "<leader>j", "*``cgn", opts)
keymap(
    "x",
    "<leader>j",
    "y<cmd>let @/=substitute(escape(@\", '/'), '\\n', '\\\\n', 'g')<CR>\"_cgn",
    opts
)

-- press q to exit help buffer
vim.cmd("autocmd FileType help nnoremap <buffer> q :q<CR>")

-- highlight on yank
local highlight_group =
    vim.api.nvim_create_augroup("YankHighlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
    callback = function()
        vim.highlight.on_yank()
    end,
    group = highlight_group,
    pattern = "*",
})

-- basic control buffers
keymap("n", "[b", ":bN<CR>", { desc = "[[]previous [B]uffer" })
keymap("n", "]b", ":bn<CR>", { desc = "[[]next [B]uffer" })

-- navigate windows, resize
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- stay indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- telescope
keymap( "n", "<leader>?", require("telescope.builtin").oldfiles, { desc = "[?] Find recently opened files" })
keymap( "n", "<leader><space>", require("telescope.builtin").buffers, { desc = "[ ] Find existing buffers" })
keymap("n", "<leader>/", function() require("telescope.builtin").current_buffer_fuzzy_find( require("telescope.themes").get_dropdown({ winblend = 10, previewer = false, })) end, { desc = "[/] Fuzzily search in current buffer" })
keymap( "n", "<leader>gf", require("telescope.builtin").git_files, { desc = "Search [G]it [F]iles" })
keymap( "n", "<leader>sf", require("telescope.builtin").find_files, { desc = "[S]earch [F]iles" })
keymap( "n", "<leader>sh", require("telescope.builtin").help_tags, { desc = "[S]earch [H]elp" })
keymap( "n", "<leader>sw", require("telescope.builtin").grep_string, { desc = "[S]earch current [W]ord" })
keymap( "n", "<leader>sg", require("telescope.builtin").live_grep, { desc = "[S]earch by [G]rep" })
keymap( "n", "<leader>sd", require("telescope.builtin").diagnostics, { desc = "[S]earch [D]iagnostics" })

-- diagnostic
keymap( "n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
keymap( "n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
keymap( "n", "<leader>e", vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
keymap( "n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })
