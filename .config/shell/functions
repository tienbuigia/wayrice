#!/bin/sh
#
# functions
#
# shellcheck disable=2046

[ "$(command -v fdfind)" ] && fd=fdfind || fd=fd

op() {
  test -z "$1" && dir="$($fd -I -t d . | fzf)" || dir="$1"
  [ -d "$dir" ] && cd "$dir" && ls -hA
}

e() {
  test -z "$1" && file="$($fd -I -t f . | fzf)" || file="$1"
  [ -f "$file" ] && $EDITOR "$file"
}

o() {
  test -z "$1" && file="$($fd -I -t f . | fzf)" || file="$1"
  [ -f "$file" ] && xdg-open "$file"
}

# for hidden files and directories
oph() {
  test -z "$1" && dir="$(find "$HOME/.config" "$HOME/.local" -not -path '*.git*' -type d | fzf)" || dir="$1"
  [ -d "$dir" ] && cd "$dir" && ls -hA
}

eh() {
  test -z "$1" && file="$(find "$HOME/.config" "$HOME/.local" -type f -not -path '*.git*' | fzf)" || file="$1"
  [ -f "$file" ] && $EDITOR "$file"
}

oh() {
  test -z "$1" && file="$(find "$HOME/.config" "$HOME/.local" -type f -not -path '*.git*' | fzf)" || file="$1"
  [ -f "$file" ] && xdg-open "$file"
}

# formating stardict
def() {
  sdcv -n --utf8-output --color "$@" 2>&1 | \
    fold --width=$(tput cols) | \
    less --quit-if-one-screen -RX
}

rmfzf() {
  fzf -m --print0 | xargs -0 rm -v
}

mvfzf() {
  files=$(fzf -m --prompt 'Sources: ')
  [ -z "$files" ] && return 1

  dest=$(find . -type d 2>/dev/null | fzf --prompt 'Destination: ')
  [ -z "$dest" ] && return 1

  echo "$files" | while read -r file; do
    mv -iv "$file" "$dest"
  done
}

cpfzf() {
  files=$(fzf -m --prompt 'Sources: ')
  [ -z "$files" ] && return 1

  dest=$(find . -type d 2>/dev/null | fzf --prompt 'Destination: ')
  [ -z "$dest" ] && return 1

  echo "$files" | while read -r file; do
    cp -iv "$file" "$dest"
  done
}

nman() {
  nvim "+hide Man $*"
}
