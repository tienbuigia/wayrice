#!/bin/sh

if [ "$(hyprctl getoption general:gaps_in | cut -d' ' -f3)" = "0" ]; then
  hyprctl keyword general:gaps_in 3
  hyprctl keyword general:gaps_out 5
else
  hyprctl keyword general:gaps_in 0
  hyprctl keyword general:gaps_out 0
fi
