#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# export XINITRC="$HOME/.config/x11/xinitrc"
[ -f "$HOME/.config/shell/profile" ] && source "$HOME/.config/shell/profile" 

if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" -eq 1 ]; then
    # exec startx "$XINITRC"
    # exec dwlst
    exec Hyprland
fi

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
. "$HOME/.cargo/env"
