#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\[\e[91m\]\u\[\e[0m\]@\[\e[93m\]\h \[\e[92m\]\W\[\e[0m\]] \[\e[95m\]$(git branch 2>/dev/null | grep \* | cut -d" " -f2)\[\e[0m\]\$ '

. "$HOME/.config/shell/aliases"
. "$HOME/.config/shell/functions"

export HISTCONTROL="erasedups:ignorespace"
set -o vi
shopt -s autocd

complete -c which man whatis whereis command
complete -F _longopt op

sed '/^x/d' ~/todo.txt
toilet -f term "I'm on sale!" --filter border

# Shell integration with foot
# https://codeberg.org/dnkl/foot/wiki#piping-last-command-s-output
PS0+='\e]133;C\e\\'
command_done() {
    printf '\e]133;D\e\\'
}
PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }command_done

[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"
