#!/bin/sh
#
# ┌──────────────────────────────────────────────────────┐
# │█                    █                    ▀▀█    ▀▀█  │
# │█ ▄▄    ▄▄▄    ▄▄▄   █ ▄▄  ▄     ▄  ▄▄▄     █      █  │
# │█▀  █  ▀   █  █   ▀  █▀  █ ▀▄ ▄ ▄▀ ▀   █    █      █  │
# │█   █  ▄▀▀▀█   ▀▀▀▄  █   █  █▄█▄█  ▄▀▀▀█    █      █  │
# │█   █  ▀▄▄▀█  ▀▄▄▄▀  █   █   █ █   ▀▄▄▀█    ▀▄▄    ▀▄▄│
# └──────────────────────────────────────────────────────┘
#
# copied from this guy https://git.pyratebeard.net/dotfiles/
# his scripts based on bash specific features a lot
# so I had to changed some places to comply with POSIX (I use dash)
#
# dependencies: setbg (my other tool), imagemagick

usage() {
  cat <<EOF
Usage: ${0##*/} [-bfsr]
  -b background color (#222222)
  -f foreground color (#3e3e3e)
  -s step (2)
  -r resolution (24)
EOF
  exit 0
}

#args
res=24
step=4
bg="#222222"
fg="#3e3e3e"

# evaluate
while [ $# -gt 0 ]; do
  case "$1" in
    -b)
      bg="$2"
      shift
      ;;
    -f)
      fg="$2"
      shift
      ;;
    -s)
      step="$2"
      shift
      ;;
    -r)
      res="$2"
      shift
      ;;
    --)
      # I don't understand this dash dash
      # is this just for compatibility?
      # Oh, this is for stopping parsing
      shift
      break
      ;;
    -*)
      usage
      ;;
    *)
      break
      ;;
  esac
  shift
done

# calculate
max=$((res * 2 + 2))
i=0
lines=""
while [ "$i" -lt "$max" ]; do
  lines="$lines line $i,-1 -1,$i"
  i="$((i + step))"
done

# generate
convert -size "$res"x"$res" xc:"$bg" -stroke "$fg" -draw "$lines" /tmp/wall.png
setbg -m tile /tmp/wall.png
